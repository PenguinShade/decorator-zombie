using System.Collections.Generic;

namespace zombies_game
{
    using System;
    public class GameObjectManager
    {
        //Keep track of all the enemies.
        //Accessed by GameEventManger when calculating collision.
        //The List here is filled when the user creates the Zombies.
        private List<Zombie> zombies;
        //TODO: Observer Pattern related attributes and methods.
        public Zombie frontZombie
        {
            get
            {
                if (zombies.Count > 0) //Guard code
                {
                    // if (!zombies[0].isAlive())
                    // {
                    //     zombies.RemoveAt(0);
                    //     return frontZombie;
                    // }
                    return zombies[0];
                }
                return null;
            }
        }

        public GameObjectManager()
        {
            zombies = new List<Zombie>();
        }

        public void handleZombieDeath()
        {
            // Console.WriteLine("zombie died from gom\n");
            if (!zombies[0].isAlive())
            {
                zombies.RemoveAt(0);
            }
        }

        public void AddZombie(Zombie zombie)
        {
            zombie.onDeath += handleZombieDeath;
            zombies.Add(zombie);
        }

        public void printZombies()
        {
            Console.Write("[");
            Console.Write(String.Join(", ", zombies));
            Console.WriteLine("]");
        }
    }
}