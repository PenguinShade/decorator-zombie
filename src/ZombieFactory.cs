using System;

namespace zombies_game {
    public class ZombieFactory {
        public Zombie createZombie(string accessoryType) {
            Zombie z = new ConcreteZombie(50);
            if(accessoryType.CompareTo("cone") == 0)
                z = new Cone(z, 25);
            else if(accessoryType.CompareTo("bucket") == 0)
                z = new Bucket(z, 100);
            else if(accessoryType.CompareTo("screen door") == 0)
                z = new ScreenDoor(z, 25);
            else if(accessoryType.CompareTo("none") == 0)
                ;
            else
                throw new System.Exception("Invalid accessoryType option");


            return z;
        }
    }
}