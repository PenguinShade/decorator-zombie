namespace zombies_game {
    public abstract class Zombie {
        /**
            Does damage on zombie. Returns leftover damage if killed.
         */
        public abstract int takeDamage(int damage);
        /**
            Takes damage from the top. Only applies to watermelon attacks and the normal/screendoor zombie.
         */
        public abstract int takeTopDamage(int damage);
        /**
            Returns string of the type of zombie with health. Like R/70   
         */
        public abstract string getType();
        /***
            Does magenetic attack. Returns true if it worked, else false.
         */
        public abstract bool magneticAttack();
        /**
            Bool with if the zombie is alive.
         */
        public abstract bool isAlive();
        public delegate void raiseDeathEvent();
        public virtual event raiseDeathEvent onDeath;
    }
}