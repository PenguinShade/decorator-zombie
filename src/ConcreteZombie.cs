using System;

namespace zombies_game
{
    public class ConcreteZombie : Zombie
    {
        private int hitpoints;

        public override event raiseDeathEvent onDeath;

        public ConcreteZombie(int hitpoints)
        {
            this.hitpoints = hitpoints;
        }

        private void die()
        {
            // throw new System.NotImplementedException();
            Console.WriteLine("arg I died\n");
            onDeath();
        }

        public override int takeDamage(int d)
        {
            hitpoints -= d;
            if (hitpoints <= 0)
            {
                die();
                return -hitpoints;
            }
            return 0;
        }


        public override string ToString() {
            return "R/"+ (hitpoints).ToString();
        }

        public override string getType()
        {
            throw new NotImplementedException();
        }

        public override bool magneticAttack()
        {
            //Do nothing
            return false;
        }

        public override bool isAlive()
        {
            return hitpoints > 0;
        }

        public override int takeTopDamage(int damage)
        {
            return takeDamage(damage);
        }


    }
}