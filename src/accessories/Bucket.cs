using System;

namespace zombies_game
{

    public class Bucket : Accessory 
    {


        public Bucket(Zombie zombieReference, int hitpoints) : base(zombieReference, hitpoints)
        {
            this.hitpoints = hitpoints;
            this.isActive = true;
            this.typeName = "Bucket";
        }


        public override bool magneticAttack()
        {
            //FIXME: Recursive call if two bucket hats
            if(isActive)
            {
                die();
                return true;
            }
            else
            {
                return zombieReference.magneticAttack();
            }
        }
    }
}