using System;

namespace zombies_game
{

    public class Cone : Accessory 
    {


        public Cone(Zombie zombieReference, int hitpoints) : base(zombieReference, hitpoints)
        {
            this.hitpoints = hitpoints;
            this.isActive = true;
            this.typeName = "Cone";
        }


        public override bool magneticAttack()
        {
            return false;
        }
    }
}