namespace zombies_game {
    using System;
    public abstract class Accessory : ZombieDecorator {

        protected string typeName;
        protected bool isActive;
        protected int hitpoints;
        public override event raiseDeathEvent onDeath {
            add {this.zombieReference.onDeath += value;}
            remove {this.zombieReference.onDeath -= value;}
        }
        public Accessory(Zombie zombieReference, int hitpoints) : base(zombieReference)
        {
            this.zombieReference = zombieReference;
            this.isActive = true;
            // this.onDeath = zombieReference.onDeath;
        }

        public void die()
        {
            Console.WriteLine("Bucket fell off");
            isActive = false;
        }

        public override string getType()
        {
            return isActive ? "Bucket" : zombieReference.getType();
        }

        public override bool isAlive()
        {
            return isActive || zombieReference.isAlive();
        }

        public override int takeDamage(int damage)
        {
            int leftover;
            if (isActive)
            {
                hitpoints -= damage;
                if (hitpoints <= 0)
                {
                    die();
                    //Send leftover damage to zombieReference (will either be zombie or accessory)
                    leftover = zombieReference.takeDamage(-hitpoints);
                    return leftover;
                }
                return 0;
            }
            else
            {
                leftover = zombieReference.takeDamage(damage);
                return leftover;
            }
        }

        public override int takeTopDamage(int damage)
        {
            return takeDamage(damage);
        }

        public override string ToString()
        {
            if(isActive)
                return typeName[0] + "/" + hitpoints.ToString();
            else
                return zombieReference.ToString();
        }
    }
}