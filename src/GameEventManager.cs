namespace zombies_game
{

    public class GameEventManager
    {
        private GameObjectManager gomRef;
        public GameEventManager(GameObjectManager gameObjectManager)
        {
            gomRef = gameObjectManager;
        }
        //Called when collision is detected between a Bullet b and an Enemy e
        //In a real implementation, the operation will get the damage from the
        //Bullet b. For instance:
        //public void doDamage(Bullet b, Enemy e)
        //{
        // int damage = b.getDamage();
        // e.takeDamage(damage);
        //}
        //For this test, simply pass in the damage of the Peashooter/Watermelon
        //to the following operation (25 or 30, specifically)
        public void doDamage(int d, Zombie e)
        {
            e.takeDamage(d);
        }
        public void doTopDamage(int d, Zombie e)
        {
            e.takeTopDamage(d);
        }
        //Called when "collision" is detected between
        //a magnet-shroom and an Enemy e
        //i.e, when the user select the magnet-shroom attack.
        public void applyMagnetForce(Zombie e)
        {
            //If it's a metal accessory, we need to remove the “metal” accessory
            //from e. How? It’s up to you
            //TODO: complete this method.
            //Hint: a simple type-check is necessary here.
            e.magneticAttack();
        }
        //To separate the responsibilities, the above methods should not
        //be called directly from your code handling user-interaction.
        //Instead, it should be done in this “hub” operation in the control
        //class. Since we are simulating, pass an “int” to represent the plant.
        public void simulateCollisionDetection(int plant)
        {
            //The method gets access to the “enemies” list in GameObjectManager
            //and finds the first Enemy to be the one to collide with.
            //Then, it passes e to one of the functions above.
            //TODO: complete this method.
            Zombie z = gomRef.frontZombie;
            switch(plant)
            {
                case 1: doDamage(25, z); break; //Peashooter
                case 2: doTopDamage(30, z); break; //Watermelon
                case 3: applyMagnetForce(z); break; //Magnet 
            }
        }
    }
}