namespace zombies_game
{
    using System;
    using System.Collections.Generic;
    public class Game
    {

        GameObjectManager gameObjectManager;
        GameEventManager gameEventManager;
        public void Run(string[] args)
        {
            bool running = true;
            int option = 0;
            gameObjectManager = new GameObjectManager();
            gameEventManager = new GameEventManager(gameObjectManager);
            List<Zombie> zlist = new List<Zombie>();
            while (running)
            {
                Console.WriteLine("1. Create zombies?");
                Console.WriteLine("2. Demo game play?");
                Console.WriteLine("3. Exit");
                option = getInput();
                switch (option)
                {
                    case 1: //Create zombies
                        createZombies();
                        gameObjectManager.printZombies();
                        break;
                    case 2: //Demo game play
                        demoGamePlay();
                        break;
                    case 3:
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Invalid option");
                        break;
                }
            }
        }

        void demoGamePlay()
        {
            bool running = true;
            int plant = 1;
            while (running && gameObjectManager.frontZombie != null)
            {
                gameObjectManager.printZombies();
                Console.WriteLine("1. Peashooter");
                Console.WriteLine("2. Watermelon");
                Console.WriteLine("3. Magnetic");
                Console.WriteLine("4. Exit");
                Console.Write("What attack would you like to use?: ");
                int option = getInput();
                switch (option)
                {
                    case 1:
                    case 2:
                    case 3:
                        gameEventManager.simulateCollisionDetection(option);
                        break;
                    case 4:
                        running = false;
                        break;
                }
            }
        }

        void createZombies()
        {
            bool running = true;
            ZombieFactory zombieFactory = new ZombieFactory();
            while (running)
            {
                Console.WriteLine("Which kind?");
                Console.WriteLine("1. Regular");
                Console.WriteLine("2. Cone");
                Console.WriteLine("3. Bucket");
                Console.WriteLine("4. ScreenDoor");
                Console.WriteLine("5. Exit");
                int option = getInput();
                switch (option)
                {
                    case 1: //Regular
                        gameObjectManager.AddZombie(zombieFactory.createZombie("none"));
                        break;
                    case 2: //Cone
                        gameObjectManager.AddZombie(zombieFactory.createZombie("cone"));
                        break;
                    case 3: //Bucket
                        gameObjectManager.AddZombie(zombieFactory.createZombie("bucket"));
                        break;
                    case 4: //Screen Door
                        gameObjectManager.AddZombie(zombieFactory.createZombie("screen door"));
                        break;
                    case 5: //Exit
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Invalid option");
                        break;
                }
            }
        }

        int getInput()
        {
            string optionString;
            int option = 0;
            bool validInput = false;
            while (!validInput)
            {
                optionString = Console.ReadLine();
                try
                {
                    option = Convert.ToInt16(optionString);
                    validInput = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid option");
                    continue;
                }
            }
            return option;
        }
    }
}
